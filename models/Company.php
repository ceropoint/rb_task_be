<?php

namespace app\models;

use Yii;
use yii\db\Expression;
use yii\rbac\Permission;
use yii\web\Request as WebRequest;

/**
 * Class User
 *
 * @property integer $id
 * @property string $company_name
 * @property string $company_number
 * @property string $address_line_1
 * @property string $address_line_2
 * @property string $post_town
 * @property string $contry
 * @property string $post_code
 * @property integer $url
 *
 * @package app\models
 */
class Company extends \yii\db\ActiveRecord
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'company';
    }


    /** @inheritdoc */
    public function attributeLabels()
    {
        return [
            'company_name' => Yii::t('app', 'Company Name'),
            'company_number' => Yii::t('app', 'Company Number'),
            'address_line_1' => Yii::t('app', 'Address Line 1'),
            'address_line_2' => Yii::t('app', 'Address Line 2'),
            'post_town' => Yii::t('app', 'Post Town'),
            'country' => Yii::t('app', 'Country'),
            'post_code' => Yii::t('app', 'Post Code'),
            'url' => Yii::t('app', 'Url'),
        ];
    }

    /** @inheritdoc */
    public function behaviors()
    {
        // TimestampBehavior also provides a method named touch() that allows you to assign the current timestamp to the specified attribute(s) and save them to the database. For example,
        return [

        ];
    }

    public function fields()
    {
        $fields = [
            'id',
            'company_name',
            'company_number',
            'address_line_1',
            'address_line_2',
            'post_town',
            'country',
            'post_code',
            'url'
        ];
    }


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [

            ['company_name', 'string', 'max' => 255],
            [['company_name', 'company_number'], 'safe'],
        ];
    }
}
