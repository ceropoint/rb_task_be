<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;

class CompanySearch extends Model
{

    public $q;
    public $page = 1;
    public $per_page = 20;

    public function rules()
    {
        return [
            [['page', 'per_page'], 'integer'],
            [['q'], 'string', 'max' => 50],
        ];
    }





    public function formName()
    {
        return '';
    }

    public function getDataProvider()
    {
        $queryParams = [];
        $query = Company::find();
        //    ->where(['not', 'company.company_name = null']);

        if ($this->q) {
            $query->andWhere([
                'or',
                ['like', 'company.company_name', '%'.$this->q.'%',false],
                ['like', 'company.company_number', '%'.$this->q.'%',false],
            ]);
            $queryParams['q'] = $this->q;
        }

        $page = $this->page > 0 ? ($this->page - 1) : 0;
        $pageSize = (int)$this->per_page;

        $provider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'forcePageParam' => true,
                'page' => $page,
                'pageParam' => 'page',
                'defaultPageSize' => $pageSize,
                'pageSizeLimit' => [1, 100],
                'pageSizeParam' => $this->per_page,
                'validatePage' => true,
                'params' => $queryParams,
            ],
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_DESC,
                ]
            ]
        ]);

        $rows = $provider->getModels();

        $pagination = array_intersect_key(
            (array)$provider->pagination,
            array_flip(
                \Yii::$app->params['paginationParams']
            )
        );

        $pagination['firstRowNo'] = $pagination['totalCount'] - ($page * $pageSize);



        $data = ArrayHelper::toArray($rows, [
            'app\models\Company' => [
                'id',
                'company_name',
                'company_number',
                'address_line_1',
                'address_line_2',
                'post_town',
                'country',
                'post_code',
                'url'
            ],
        ]);



        return [
            'rows' => $data,
            'pagination' => $pagination,
        ];
    }
}
