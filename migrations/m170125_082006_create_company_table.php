<?php

use yii\db\Migration;

/**
 * Handles the creation of table `company`.
 */
class m170125_082006_create_company_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function up()
    {
        $this->createTable(
            'company',
            [
                'id' => $this->primaryKey(),
                'company_name' => $this->string(255),
                'company_number' => $this->string(255),
                'address_line_1' => $this->string(255),
                'address_line_2' => $this->string(255),
                'post_town' => $this->string(255),
                'country' => $this->string(255),
                'post_code' => $this->string(255),
                'url' => $this->string(255),
            ]
        );

    }

    /**
     * {@inheritdoc}
     */
    public function down()
    {

        $this->dropTable('company');
    }
}
