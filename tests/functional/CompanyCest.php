<?php

namespace tests\functional;

class CompanyCest
{
    public function _before(\FunctionalTester $I)
    {
    }

    public function testGetCompany(\FunctionalTester $I)
    {
        $I->haveHttpHeader('Content-Type', 'application/json');

        $I->sendGET('http://api.rb-local.com/v1/company?q=sdl', []);
        $I->canSeeResponseCodeIs(200);
        $I->seeResponseIsJson();
        $I->seeResponseContainsJson([
            'success' => true,
            'status' => 200,
            'data' => [
                'rows' =>[],

            ]
        ]);
    }
}
